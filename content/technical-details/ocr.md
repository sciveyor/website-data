---
title: OCR Process
weight: 201
---

# OCR Process

A number of our data sources have been processed directly from downloaded PDFs,
which has thus frequently required the conversion of those files to plain-text
via optical character recognition (OCR). This page documents the ways in which
we have performed OCR in the past and present. It is hoped that this information
could help users understand something about our rates and types of OCR error.

## Current OCR Pipeline

We begin by converting PDF files to 600-DPI greyscale TIFF images, then OCRing
those using the [Tesseract](https://github.com/tesseract-ocr/tesseract) OCR
system. This produces plain text files with quality comparable to, in our
testing, other top-end commercial and non-commercial OCR systems (including
ABBYY FineReader, our previous OCR solution for which we no longer have a
license). We are currently using Tesseract version 5.2.0, with the most recently
available official Tesseract English-language data (currently 4.1.0; this is
rarely updated at Tesseract and may be a weak point in our workflow).

We then post-process the resulting text-files to fix known problems that
Tesseract introduces. The full details and source code of those post-processing
scripts can be found in
[our scripts repository](https://codeberg.org/sciveyor/scripts), but to
summarize them briefly (there is currently only one such script):

- Fix the fact that Tesseract does not join hyphenated words at the end of lines
  by merging those words. This process is also sensitive to the possibility of
  hyphenated compound words occurring across a line-break.

{{<details "Changelog">}}

## Changelog

### v1.1: 2022-08-08

- Slight updates to version numbers of Tesseract and training data.

### v1.0: 2021-06-15

- Initial new OCR workflow with Tesseract 5-alpha and one processing script.

### v0 (during the evoText project)

Note that this pre-Sciveyor process applies _only_ to data from the journal
_Nature_ prior to 2011-07-14.

- OCR with ABBYY FineReader 11
- No post-processing of files

{{</details>}}
