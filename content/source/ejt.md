---
title: European Journal of Taxonomy
---

# European Journal of Taxonomy

This is a canonical data source description,
`https://data.sciveyor.com/source/ejt`.  
The current `dataSourceVersion` described by this documentation is 1. The
`dataSource` name for this data is `European Journal of Taxonomy`.

**Coverage:** _European Journal of Taxonomy,_ from first publication (September 2011) to August 4, 2022  
**Size:** 1,105 articles  
**Copyright:** Copyright the author and the Museum National d'Histoire Naturelle  
**License:** [CC-BY 4.0](http://creativecommons.org/licenses/by/4.0/) **Credits:** Stijn Conix and C.H. Pence

## How we got it

PDFs for these articles were downloaded directly from the journal's website.

## Processing

- **Bibliographic Information:** Crossref
- **PMIDs, PMCIDs, and PubMed Manuscript IDs:**
  [PubMed scraping]({{< relref "../technical-details/pubmed-scraping.md" >}})
- **Full text:** extracted directly from PDFs
- **Keywords and Tags:** "Subject" tags saved in Crossref have been preserved as
  tags.

## Changelog

- **Data Source Version 1 (2021-08-29):** initial content import.
