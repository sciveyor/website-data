---
title: Journal of Genetics
---

# Journal of Genetics

This is a canonical data source description,
`https://data.sciveyor.com/source/jgen`.  
The current `dataSourceVersion` described by this documentation is 1. The
`dataSource` name for this data is `Journal of Genetics`.

**Coverage:** _Journal of Genetics,_ from first publication (November 1910) to August 4, 2022  
**Size:** 3,671 articles  
**Copyright:** Copyright Springer Nature  
**License:** [Springer Nature TDM License](https://www.springernature.com/gp/researchers/text-and-data-mining)  
**Credits:** C.H. Pence and Nicola Bertoldi

## How we got it

PDFs for these articles were downloaded directly from Springer Nature, in
accordance with their TDM policy.

## Processing

- **Bibliographic Information:** Crossref
- **PMIDs, PMCIDs, and PubMed Manuscript IDs:**
  [PubMed scraping]({{< relref "../technical-details/pubmed-scraping.md" >}})
- **Full text:**
  - For articles from early-1999 and earlier (PDFs that contain scanned images
    rather than native digital text): [OCR]({{< relref
    "../technical-details/ocr.md" >}})
  - Articles from late-1999 and later (with native digital text): text was
    extracted directly from PDFs
- **Keywords and Tags:** "Subject" tags saved in Crossref have been preserved as
  tags.

## Changelog

- **Data Source Version 1 (2021-08-29):** initial content import.
