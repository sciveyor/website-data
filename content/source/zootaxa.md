---
title: Zootaxa
---

# Zootaxa

This is a canonical data source description,
`https://data.sciveyor.com/source/zootaxa`.

The current `dataSourceVersion` described by this documentation is 1. The
`dataSource` name for this data is `Zootaxa`.

**Coverage:** _Zootaxa,_ from its founding until April 14, 2021  
**Size:** 31,348 articles  
**Copyright:** articles are copyright Magnolia Press, appearing under agreement
with the publisher  
**Credits:** Stijn Conix, C.H. Pence

## How we got it

This data was provided to us via a bulk download from Magnolia Press. We cannot
extend our coverage without obtaining further articles from them.

## Processing

- **Plain Text:** Extracted from journal article PDF files.
- **Metadata:** JSON files of metadata provided by the publisher, supplemented
  by [CrossRef scraping]({{<relref "/technical-details/crossref-scraping.md">}})
- **Keywords and Tags:** Many papers have publisher-provided "subject" tags,
  which were preserved in the `tags` field.

## Changelog

- **Data Source Version 3 (2022-08-04):** Remove `null` values from JSON.
- **Data Source Version 2 (2022-08-04):** Fix missing `licenseUrl` parameter.
- **Data Source Version 1 (2022-08-04):** First import of Zootaxa data.
