---
title: Eugenics Review
---

# Eugenics Review

This is a canonical data source description,
`https://data.sciveyor.com/source/eugenrev`.  
The current `dataSourceVersion` described by this documentation is 2. The
`dataSource` name for this data is `Eugenics Review`.

**Coverage:** The journal _Eugenics Review,_ from its founding in 1909 until its
dissolution and renaming in 1968  
**Size:** 4,003 articles  
**Copyright:** PubMed Central  
**License:** freely available from PubMed Central, courtesy of The Galton
Institute  
**Credits:** C.H. Pence, Nicola Bertoldi

## How we got it

These articles were downloaded directly from the
[PubMed Central website](https://www.ncbi.nlm.nih.gov/pmc/journals/1186/)
containing the _Eugenics Review_ archive. All metadata was also collected from
this same source (it is supplied in the HTML pages as `<meta>` tags).

## Processing

- **OCR to plain text:** Unknown, performed by PMC. Plain text was extracted
  directly from the provided PDFs.
- **Metadata:** Extracted from PMC pages.
- **Canonical JSON:** Created directly from metadata and full text.
- **Keywords and Tags:** There are no keywords or tags in this data source.
- **Cleaning:** PDF files containing front matter, back matter, advertisements,
  or summaries of other literature do not have metadata available in PMC, and so
  were excluded.

## Changelog

- **Data Source Version 2 (2022-05-10):** Day-zero bugfix release for author
  name parsing.
- **Data Source Version 1 (2022-05-10):** First import of entire print run
  directly from PMC.
