# Sciveyor Data Documentation

This repository contains the source for the Sciveyor data documentation website,
located at <https://data.sciveyor.com>. The source of this website is released
under the [CC-BY 4.0 license.](https://creativecommons.org/licenses/by/4.0/)

Unless otherwise specified, all content here is copyright
[Charles H. Pence](https://www.charlespence.net) and the Sciveyor team.
